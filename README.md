# README #

This is the technical test repository. Please follow below set up steps to run it.

### How do I get set up? ###

* Clone develop branch.
* Run 'pod install' in terminal with cocoapod 1.0+.
* Open PhotoLocations.xcworkspace in Xcode8.
* To run the project, Choose PhotoLocations target and any simulator or device to run. If run in simulator, please go to **Debug->Location->Customer Location** and set *-33.860178* as latitude, *151.212706* as longitude to use Sydney as current user location.
* To run test cases, go to Test navigator and run the test cases.
* Realm is used

### Assumptions ###

* Every time when App launches, the default locations are added into Database if they are not yet.
* Long press anywhere on the map, a dialogue box is shown to create a custom location.
* Click on pin on the map, a callout will show the location's name and distance to you. Tapping the callout brings you to its detail view to edit note.
* Switch between map and list via top right corner button.
* The center button on map will help you find your current location.
* Tap map button in each cell in the list brings you to its pin on map.
* Tap a cell in list will show edit note view.

### Who do I talk to? ###

* Please contact Can at canzhan@gmail.com with any query.