//
//  LocationManagerTests.swift
//  PhotoLocations
//
//  Created by Can on 11/12/16.
//  Copyright © 2016 Can. All rights reserved.
//

import XCTest
import RealmSwift
@testable import PhotoLocations

class LocationManagerTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        let realm = try! Realm()
        try! realm.write {
            realm.deleteAll()
        }
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testFetchDefaultLocations() {
        XCTAssert(LocationManager.fetchDefaultLocations().count == 5)
    }
    
    func testSaveDefaultLocationsIfNeeded() {
        XCTAssert(LocationManager.fetchLocations().isEmpty)
        // call multiple times should still have only one copy saved
        LocationManager.saveDefaultLocationsIfNeeded()
        LocationManager.saveDefaultLocationsIfNeeded()
        LocationManager.saveDefaultLocationsIfNeeded()
        XCTAssert(LocationManager.fetchLocations().count == LocationManager.fetchDefaultLocations().count)
    }
    
    func testSaveLocation() {
        XCTAssert(LocationManager.fetchLocations().isEmpty)
        LocationManager.save(location: Location())
        XCTAssert(LocationManager.fetchLocations().count == 1)
        LocationManager.save(location: Location())
        XCTAssert(LocationManager.fetchLocations().count == 2)
    }
    
    func testSaveLocations() {
        XCTAssert(LocationManager.fetchLocations().isEmpty)
        LocationManager.save(locations: [Location(), Location(), Location()])
        XCTAssert(LocationManager.fetchLocations().count == 3)
        LocationManager.save(locations: [Location(), Location(), Location()])
        XCTAssert(LocationManager.fetchLocations().count == 6)
    }
    
    func testDeleteLocation() {
        XCTAssert(LocationManager.fetchLocations().isEmpty)
        let location = Location()
        LocationManager.save(location: location)
        XCTAssert(LocationManager.fetchLocations().count == 1)
        LocationManager.delete(location: location)
        XCTAssertTrue(location.isInvalidated)
        XCTAssert(LocationManager.fetchLocations().isEmpty)
    }
    
    func testUpdateLocation() {
        XCTAssert(LocationManager.fetchLocations().isEmpty)
        let location = Location()
        LocationManager.save(location: location)
        XCTAssert(LocationManager.fetchLocations().first?.note == nil)
        let note1 = "note1"
        LocationManager.update(location: location, withNote: note1)
        XCTAssert(LocationManager.fetchLocations().first?.note == note1)
        let note2 = "note2"
        LocationManager.update(location: location, withNote: note2)
        XCTAssert(LocationManager.fetchLocations().first?.note == note2)
    }
    
}
