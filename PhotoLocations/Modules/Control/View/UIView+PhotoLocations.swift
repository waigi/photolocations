//
//  UIView+PhotoLocations.swift
//  PhotoLocations
//
//  Created by Can on 12/12/16.
//  Copyright © 2016 Can. All rights reserved.
//

import UIKit

extension UIView {
    
    var superviewCell: UITableViewCell? {
        var result: UITableViewCell?
        var view: UIView? = self
        while view != nil {
            if let possible = view as? UITableViewCell {
                result = possible
                break
            } else {
                view = view?.superview
            }
        }
        return result
    }
    
}
