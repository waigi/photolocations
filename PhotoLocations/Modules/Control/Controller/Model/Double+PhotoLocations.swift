//
//  Double+PhotoLocations.swift
//  PhotoLocations
//
//  Created by Can on 11/12/16.
//  Copyright © 2016 Can. All rights reserved.
//

import Foundation

extension Double {
    
    /// Formatted distance in KM string. e.g "1.23 KM"
    var distanceInKMsStringFromMeters: String {
        let formatter = NumberFormatter()
        formatter.minimumIntegerDigits = 1
        formatter.maximumFractionDigits = 2
        if let string = formatter.string(from: NSNumber(value: self / 1000.0)) {
            return string + " KM"
        } else {
            return ""
        }
    }
    
}
