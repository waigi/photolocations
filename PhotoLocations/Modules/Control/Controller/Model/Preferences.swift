//
//  Preferences.swift
//  PhotoLocations
//
//  Created by Can on 11/12/16.
//  Copyright © 2016 Can. All rights reserved.
//

import Foundation
import MapKit

/// User settings
struct Preferences {
    
    private static let userDefaults = UserDefaults.standard
    
    private struct Key {
        static let mapCenter = "mapCenter"
        static let latitude = "latitude"
        static let longitude = "longitude"
        static let mapSpan = "mapSpan"
        static let latitudeDelta = "latitudeDelta"
        static let longitudeDelta = "longitudeDelta"
        static let userLocation = "userLocation"
    }
    
    private struct Constant {
        static let defaultLatitudeDelta = 0.06
        static let defaultLongitudeDelta = 0.06
    }
    
    /// Save latest location on map
    static func setLatestMapCenter(coordinate: CLLocationCoordinate2D) {
        let dictionary = [Key.latitude: Double(coordinate.latitude), Key.longitude: Double(coordinate.longitude)]
        userDefaults.set(dictionary, forKey: Key.mapCenter)
        userDefaults.synchronize()
    }
    
    /// Get saved latest location on map. Return nil if it has never been saved before.
    static func getLatestMapCenter() -> CLLocationCoordinate2D? {
        let coordinate: CLLocationCoordinate2D?
        if let dictionary = userDefaults.dictionary(forKey: Key.mapCenter),
            let latitude = dictionary[Key.latitude] as? Double,
            let longitude = dictionary[Key.longitude] as? Double
        {
            coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        } else {
            coordinate = nil
        }
        return coordinate
    }
    
    // Check if there is a saved location
    static func isMapCenterSaved() -> Bool {
        return userDefaults.dictionary(forKey: Key.mapCenter) != nil
    }
    
    // Save latest or default region span
    static func setMapRegion(span: MKCoordinateSpan) {
        let dictionary = [Key.latitudeDelta: Double(span.latitudeDelta), Key.longitudeDelta: Double(span.longitudeDelta)]
        userDefaults.set(dictionary, forKey: Key.mapSpan)
        userDefaults.synchronize()
    }
    
    // Get saved latest or default region span
    static func getMapRegionSpan() -> MKCoordinateSpan {
        let span: MKCoordinateSpan
        if let dictionary = userDefaults.dictionary(forKey: Key.mapSpan),
            let latitudeDelta = dictionary[Key.latitudeDelta] as? Double,
            let longitudeDelta = dictionary[Key.longitudeDelta] as? Double
        {
            span = MKCoordinateSpanMake(latitudeDelta, longitudeDelta)
        } else {
            span = MKCoordinateSpanMake(Constant.defaultLatitudeDelta, Constant.defaultLongitudeDelta)
        }
        return span
    }
    
    /// Save user location
    static func setUser(location: CLLocationCoordinate2D) {
        let dictionary = [Key.latitude: Double(location.latitude), Key.longitude: Double(location.longitude)]
        userDefaults.set(dictionary, forKey: Key.userLocation)
        userDefaults.synchronize()
    }
    
    // Check if there has a user location ever been saved
    static func isUserLocationSaved() -> Bool {
        return userDefaults.dictionary(forKey: Key.userLocation) != nil
    }

}
