//
//  LocationManager.swift
//  PhotoLocations
//
//  Created by Can on 11/12/16.
//  Copyright © 2016 Can. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift

struct LocationManager {
    
    private static let defaultLocationJsonFileURL = Bundle.main.url(forResource: "locations", withExtension: "json")
    
    // MARK: Default Locations
    
    /// Fetch default locations from JSON file
    static func fetchDefaultLocations() -> [Location] {
        var locations = [Location]()
        guard let fileURL = defaultLocationJsonFileURL else { return locations }
        do {
            let data = try Data(contentsOf: fileURL, options: .uncached)
            let json = JSON(data: data)
            if let locationsJson = json["locations"].array {
                locations = locationsJson.flatMap { Location(json: $0) }
            }
        } catch {
            print("Failed to read location.json")
        }
        return locations
    }
    
    /// If default location doesn't exist in Database, save it into.
    static func saveDefaultLocationsIfNeeded() {
        let defaultLocations = fetchDefaultLocations()
        let savedLocations = fetchLocations()
        var unsavedLocations = [Location]()
        defaultLocations.forEach { (location) in
            let saved = savedLocations.contains(location)
            if !saved {
                unsavedLocations.append(location)
            }
        }
        save(locations: unsavedLocations)
    }
    
    //MARK: Realm
    
    /// Update location
    static func update(location: Location, withNote note: String) {
        let realm = try! Realm()
        try! realm.write {
            location.note = note
        }
    }
    
    /// Persist location
    static func save(location: Location) {
        save(locations: [location])
    }
    
    /// Persist locations
    static func save(locations: [Location]) {
        guard locations.count > 0 else { return }
        let realm = try! Realm()
        try! realm.write {
            realm.add(locations)
        }
    }
    
    /// Delete location
    static func delete(location: Location) {
        let realm = try! Realm()
        try! realm.write {
            realm.delete(location)
        }
    }
    
    /// Fetch all locations
    static func fetchLocations() -> [Location] {
        let realm = try! Realm()
        return Array(realm.objects(Location.self))
    }
    
}
