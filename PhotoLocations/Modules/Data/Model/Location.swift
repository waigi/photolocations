//
//  Location.swift
//  PhotoLocations
//
//  Created by Can on 10/12/16.
//  Copyright © 2016 Can. All rights reserved.
//

import Foundation
import MapKit
import CoreLocation
import SwiftyJSON
import RealmSwift

class Location: Object, MKAnnotation {
    
    // MARK: Variables
    
    dynamic var name: String = ""
    dynamic var latitude: Double = 0.0
    dynamic var longitude: Double = 0.0
    dynamic var note: String? = nil
    
    /// pass in dynamically, no need to persistant
    var userLocation: CLLocation? {
        return CLLocationManager().location
    }
    
    // MARK: MKAnnotation
    
    var title: String? {
        return name
    }
    
    var subtitle: String? {
        return distanceToUserLocationInMeters?.distanceInKMsStringFromMeters
    }
    
    var coordinate: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
    
    // MARK: Computed Vars
    
    /// Distance between user's location and this location
    var distanceToUserLocationInMeters: Double? {
        guard let userLocation = userLocation else { return nil }
        let location = CLLocation(latitude: latitude, longitude: longitude)
        return userLocation.distance(from: location)
    }
    
    // MARK: init
    
    convenience init?(json: JSON) {
        self.init()
        guard let name = json["name"].string else { return nil }
        guard let latitude = json["lat"].double else { return nil }
        guard let longitude = json["lng"].double else { return nil }
        self.name = name
        self.latitude = latitude
        self.longitude = longitude
    }
    
    // MARK: Override
    
    override func isEqual(_ object: Any?) -> Bool {
        if let comparingLocation = object as? Location {
            return name == comparingLocation.name
        } else {
            return false
        }
    }
    
}

func ==(lhs: Location, rhs: Location) -> Bool {
    return lhs.name == rhs.name
}
