//
//  LocationViewController.swift
//  PhotoLocations
//
//  Created by Can on 12/12/16.
//  Copyright © 2016 Can. All rights reserved.
//

import UIKit
import RealmSwift

class LocationViewController: UITableViewController {
    
    // MARK: Variables
    
    var location: Location?
    var delegate: LocationUpdatedDelegate?
    
    // MARK: - IBOutlets

    @IBOutlet weak var noteTextView: UITextView!
    
    // MARK: Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = location?.name
        noteTextView.text = location?.note
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LocationViewController.hideKeyboard))
        view.addGestureRecognizer(tapGestureRecognizer)
    }
    
    // MARK: UI
    
    func hideKeyboard(recognizer: UITapGestureRecognizer) {
        if recognizer.state == .ended {
            noteTextView.resignFirstResponder()
        }
    }
    
    // MARK: IBActions
    
    @IBAction func tappedSaveButton() {
        noteTextView.resignFirstResponder()
        guard let location = location else { return }
        LocationManager.update(location: location, withNote: noteTextView.text)
        delegate?.locationUpdated()
        navigationController?.popViewController(animated: true)
    }
}
