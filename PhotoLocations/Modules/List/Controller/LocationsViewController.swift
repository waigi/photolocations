//
//  LocationsViewController.swift
//  PhotoLocations
//
//  Created by Can on 12/12/16.
//  Copyright © 2016 Can. All rights reserved.
//

import UIKit

protocol LocationUpdatedDelegate {
    /// location value modified
    func locationUpdated()
    /// location deleted
    func delete(location: Location)
}

class LocationsViewController: UITableViewController {
    
    private enum SegueIdentifier: String {
        case edit
    }
    
    private struct CellIdentifier {
        static let location = "location"
    }
    
    // MARK: Variables
    
    var locations = [Location]()
    var delegate: LocationUpdatedDelegate?
    var mapViewDelegate: MapViewDelegate?
    var selectedLocation: Location?
    
    // MARK: Life Cycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        locations.sort {
            guard let distance1 = $0.distanceToUserLocationInMeters else { return false }
            guard let distance2 = $1.distanceToUserLocationInMeters else { return true }
            return distance1 < distance2
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let selectedLocation = selectedLocation,
            let index = locations.index(of: selectedLocation)
        {
            tableView.selectRow(at: IndexPath(row: index, section: 0), animated: true, scrollPosition: .middle)
        }
    }
    
    // MARK: IBActions
    
    @IBAction func tappedShowOnMapButton(_ button: UIButton) {
        if let cell = button.superviewCell,
            let indexPath = tableView.indexPath(for: cell)
        {
            let selectedLocation = locations[indexPath.row]
            dismiss(animated: true, completion: {
                self.mapViewDelegate?.centerMapTo(location: selectedLocation)
            })
        }
    }
    
    @IBAction func tappedShowMapButton() {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return locations.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let location = locations[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.location, for: indexPath) as! LocationTableViewCell
        cell.nameLabel.text = location.name
        cell.noteLabel.text = location.note
        cell.distanceLabel.text = location.distanceToUserLocationInMeters?.distanceInKMsStringFromMeters
        return cell
    }
    
    // MARK: TableViewDelegate
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let deletedLocation = locations.remove(at: indexPath.row)
            delegate?.delete(location: deletedLocation)
            tableView.deleteRows(at: [indexPath], with: .automatic)
            LocationManager.delete(location: deletedLocation)
        }
    }
    
    // MARK: Navigation
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        var should = true
        guard let identifier = SegueIdentifier(rawValue: identifier) else { return should }
        switch identifier {
        case .edit:
            if let row = tableView.indexPathForSelectedRow?.row {
                selectedLocation = locations[row]
                should = (selectedLocation != nil)
            }
        }
        return should
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifierString = segue.identifier,
            let identifier = SegueIdentifier(rawValue: identifierString)
            else { return }
        switch identifier {
        case .edit:
            if let locationViewController = segue.destination as? LocationViewController {
                locationViewController.location = selectedLocation
                locationViewController.delegate = self
            }
        }
    }
        
}

// MARK: LocationUpdatedDelegate

extension LocationsViewController: LocationUpdatedDelegate {
    func delete(location: Location) {}

    func locationUpdated() {
        locations = LocationManager.fetchLocations()
        locations.sort {
            guard let distance1 = $0.distanceToUserLocationInMeters else { return false }
            guard let distance2 = $1.distanceToUserLocationInMeters else { return true }
            return distance1 < distance2
        }
        tableView.reloadData()
    }
}
