//
//  LocationTableViewCell.swift
//  PhotoLocations
//
//  Created by Can on 12/12/16.
//  Copyright © 2016 Can. All rights reserved.
//

import UIKit

class LocationTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var noteLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!

}
