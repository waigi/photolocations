//
//  MapViewController.swift
//  PhotoLocations
//
//  Created by Can on 10/12/16.
//  Copyright © 2016 Can. All rights reserved.
//

import UIKit
import MapKit

protocol MapViewDelegate {
    func centerMapTo(location: Location)
}

class MapViewController: UIViewController {
    
    enum SegueIdentifier: String {
        case list, edit
    }
    
    // MARK: Variables
    
    var locations = [Location]()
    var selectedLocation: Location?
    let locationManager = CLLocationManager()

    // MARK: IBOutlets
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet var mapViewLongPressGestureRecogizer: UILongPressGestureRecognizer!
    
    // MARK: Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupMapView()
        setupLocationManager()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        refreshLocations()
    }
    
    // MARK: IBActions
    
    @IBAction func tappedUserLocationButton() {
        if let userLocation = locationManager.location {
            mapView.setCenter(userLocation.coordinate, animated: true)
        }
    }
    
    // MARK: Setup
    
    private func setupMapView() {
        mapView.delegate = self
        mapView.showsUserLocation = true
        mapView.mapType = .standard
        mapView.isZoomEnabled = true
        mapView.isScrollEnabled = true
        // Viewing Area
        var region = MKCoordinateRegion()
        if let latestLocation = Preferences.getLatestMapCenter() {
            region.center = latestLocation
        } else if let currentLocation = locationManager.location {
            region.center = currentLocation.coordinate
        }
        region.span = Preferences.getMapRegionSpan()
        mapView.setRegion(region, animated: true)
        mapViewLongPressGestureRecogizer.addTarget(self, action: #selector(MapViewController.longPressMapView(recognizer:)))
    }
    
    private func setupLocationManager() {
        locationManager.distanceFilter = kCLDistanceFilterNone
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    // MARK: Add location
    
    func longPressMapView(recognizer: UILongPressGestureRecognizer) {
        if recognizer.state == .ended {
            let touchPoint = recognizer.location(in: mapView)
            let coordinate = mapView.convert(touchPoint, toCoordinateFrom: mapView)
            showNameInputAlertViewWith(coordinate: coordinate)
        }
    }
    
    /// Show name input
    private func showNameInputAlertViewWith(coordinate: CLLocationCoordinate2D) {
        let alertController: UIAlertController = UIAlertController(title: "Add a new location?", message: "Please input location name", preferredStyle: .alert)
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        let saveAction: UIAlertAction = UIAlertAction(title: "Add", style: .default) { [weak self] action -> Void in
            guard let strongSelf = self else { return }
            if let textFields = alertController.textFields,
                let textField = textFields.first,
                let name = textField.text {
                let newLocation = Location()
                newLocation.name = name
                newLocation.latitude = coordinate.latitude
                newLocation.longitude = coordinate.longitude
                LocationManager.save(location: newLocation)
                strongSelf.locations.append(newLocation)
                strongSelf.mapView.addAnnotation(newLocation)
            }
        }
        alertController.addAction(saveAction)
        alertController.addTextField { textField -> Void in
            textField.textColor = UIColor.darkText
        }
        present(alertController, animated: true, completion: nil)
    }
    
    // MARK: Service Call
    
    /// Refresh locations and show then on the map
    func refreshLocations() {
        mapView.removeAnnotations(locations)
        locations = LocationManager.fetchLocations()
        mapView.addAnnotations(locations)
    }
    
    // MARK: Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifierString = segue.identifier,
            let identifier = SegueIdentifier(rawValue: identifierString)
            else { return }
        switch identifier {
        case .list:
            if let locationsViewController = (segue.destination as? UINavigationController)?.topViewController as? LocationsViewController {
                locationsViewController.locations = locations
                locationsViewController.selectedLocation = selectedLocation
                locationsViewController.delegate = self
                locationsViewController.mapViewDelegate = self
            }
        case .edit:
            if let locationViewController = segue.destination as? LocationViewController {
                locationViewController.location = selectedLocation
                locationViewController.delegate = self
            }
        }
    }

}

// MARK: - CLLocationManagerDelegate

extension MapViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if !Preferences.isUserLocationSaved(),
            let userLocation = locations.last
        {
            Preferences.setUser(location: userLocation.coordinate)
            mapView.setCenter(userLocation.coordinate, animated: true)
        }
    }
    
}

// MARK: - MKMapViewDelegate

extension MapViewController: MKMapViewDelegate {
    
    private struct Constants {
        /// map view subtitle annotation view identifier
        static let identifier = "subtitle"
        static let calloutWidth: CGFloat = 25.0
        static let calloutHeight: CGFloat = 25.0
    }
    
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        if !Preferences.isUserLocationSaved() {
            Preferences.setUser(location: userLocation.coordinate)
        }
    }
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        if Preferences.isUserLocationSaved() {
            Preferences.setLatestMapCenter(coordinate: mapView.centerCoordinate)
            Preferences.setMapRegion(span: mapView.region.span)
        }
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard let _ = annotation as? Location else { return nil }
        let annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: Constants.identifier) ?? MKAnnotationView(annotation: annotation, reuseIdentifier: Constants.identifier)
        annotationView.image = UIImage(named: "map pin")
        annotationView.frame = CGRect(x: annotationView.frame.origin.x, y: annotationView.frame.origin.y, width: Constants.calloutWidth, height: Constants.calloutHeight)
        annotationView.centerOffset = CGPoint(x: -(Constants.calloutWidth / 2), y:  -(Constants.calloutHeight / 2))
        annotationView.canShowCallout = true
        annotationView.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        if let selectedLocation = view.annotation as? Location {
            self.selectedLocation = selectedLocation
            performSegue(withIdentifier: SegueIdentifier.edit.rawValue, sender: view)
        }
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        guard view.annotation is Location else { return }
        UIView.animate(
            withDuration: 0.9,
            animations: {
                view.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
            }, completion: { finished in
                UIView.animate(
                    withDuration: 0.9,
                    animations: {
                        view.transform = CGAffineTransform.identity
                }, completion: { finished in
                    
                })
            })
    }
    
}

// MARK: LocationUpdatedDelegate

extension MapViewController: LocationUpdatedDelegate {
    func delete(location: Location) {
        mapView.removeAnnotation(location)
        if let index = locations.index(of: location) {
            locations.remove(at: index)
        }
    }
    
    func locationUpdated() {}
}

// MARK: MapViewDelegate

extension MapViewController: MapViewDelegate {
    func centerMapTo(location: Location) {
        mapView.setCenter(location.coordinate, animated: true)
    }
}
