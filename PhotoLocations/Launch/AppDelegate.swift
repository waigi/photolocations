//
//  AppDelegate.swift
//  PhotoLocations
//
//  Created by Can on 10/12/16.
//  Copyright © 2016 Can. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func applicationDidFinishLaunching(_ application: UIApplication) {
        // save JSON results into Database
        LocationManager.saveDefaultLocationsIfNeeded()
    }

}

